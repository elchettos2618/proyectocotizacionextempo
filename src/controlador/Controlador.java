package controlador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import modelo.Cotizaciones;
import modelo.DbCotizacion;
import vista.dlgCotizacion;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

public class Controlador implements ActionListener {

    private Cotizaciones cot;
    private dlgCotizacion vista;
    private DbCotizacion db;
    private boolean isInsertar = false;

    public Controlador(Cotizaciones cot, dlgCotizacion vista, DbCotizacion db) {
        this.cot = cot;
        this.vista = vista;
        this.db = db;
        vista.btnCancelar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnBorrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
    }

    public void limpiar() {
        vista.txtNumCotizacion.setText("");
        vista.txtDescripcion.setText("");
        vista.txtPrecio.setText("");
        vista.spnPorcentaje.setValue(10);
        vista.rdb12.setSelected(true);
        vista.lblPagoInicial.setText("");
        vista.lblPagoMensual.setText("");
        vista.lblTotalFin.setText("");
    }

    public void habilitar() {
        vista.txtNumCotizacion.setEnabled(true);
        vista.txtDescripcion.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.spnPorcentaje.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
        vista.btnBorrar.setEnabled(true);
        vista.rdb12.setEnabled(true);
        vista.rdb24.setEnabled(true);
        vista.rdb36.setEnabled(true);
        vista.rdb48.setEnabled(true);
        vista.rdb60.setEnabled(true);
    }

    public void deshabilitar() {
        vista.txtNumCotizacion.setEnabled(false);
        vista.txtDescripcion.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
        vista.spnPorcentaje.setEnabled(false);
        vista.pnlPlazos.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
        vista.btnBorrar.setEnabled(false);
        vista.rdb12.setEnabled(false);
        vista.rdb24.setEnabled(false);
        vista.rdb36.setEnabled(false);
        vista.rdb48.setEnabled(false);
        vista.rdb60.setEnabled(false);
    }

    public void cargarListaCotizacion() {
        String camposName[] = {"ID", "Num. Cotizacion", "Descripcion", "Precio", "Porcentaje", "Plazo en Meses"};
        Object datos[][];
        datos = db.cargarDatos();
        DefaultTableModel dtm = new DefaultTableModel(datos, camposName);
        vista.jtbListaAlumnos.setModel(dtm);
    }

    private void iniciarVista() {
        vista.setTitle("Cotizaciones");
        vista.setSize(504, 750);
        vista.setLocationRelativeTo(null);
        vista.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        cargarListaCotizacion();
        vista.setVisible(true);
        vista.dispose();
        System.exit(0);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

       if (e.getSource() == vista.btnCerrar) {
            int opcion = 0;
            opcion = JOptionPane.showConfirmDialog(vista, "¿ Desea Salir?", "Corizacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (opcion == JOptionPane.YES_OPTION) {
                System.exit(1);
            }
        }

        if (e.getSource() == vista.btnBorrar) {
            DbCotizacion db = new DbCotizacion();
            Cotizaciones cot = new Cotizaciones();
            int opcion = 0;

            cot.setNumCotizacion(vista.txtNumCotizacion.getText());
            opcion = JOptionPane.showConfirmDialog(vista, "¿ Desea Borrar el registro", "Cotizacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (opcion == JOptionPane.YES_OPTION) {
                try {
                    db.borrar(cot);
                    JOptionPane.showMessageDialog(vista, "Se borro el registro con exito");
                    limpiar();
                    deshabilitar();
                } catch (Exception ex) {
                }

            }
        }
        
        if (e.getSource() == vista.btnBuscar) {
            try {
                DbCotizacion db = new DbCotizacion();
                Cotizaciones cot = new Cotizaciones();
                cot.setNumCotizacion(vista.txtNumCotizacion.getText());

                cot = (Cotizaciones) db.consultar(cot.getNumCotizacion());
                if (cot.getIdCotizacion() == -1) {
                    JOptionPane.showMessageDialog(vista, "No Existe");
                    limpiar();
                    deshabilitar();
                } else {
                    habilitar();
                    vista.txtDescripcion.setText(cot.getDescripcionAutomovil());
                    vista.txtPrecio.setText(String.valueOf(cot.getPrecioAutomovil()));

                    switch (cot.getPlazo()) {
                        case 12:
                            vista.rdb12.setSelected(true);
                            break;
                        case 24:
                            vista.rdb24.setSelected(true);
                            break;
                        case 36:
                            vista.rdb36.setSelected(true);
                            break;
                        case 48:
                            vista.rdb48.setSelected(true);
                            break;
                        case 60:
                            vista.rdb60.setSelected(true);
                            break;
                    }
                    vista.spnPorcentaje.setValue(cot.getPorcentajePago());
                    vista.lblPagoInicial.setText(String.valueOf(cot.calcularPagoInicial()));
                    vista.lblTotalFin.setText(String.valueOf(cot.calcularTotalAFinanciar()));
                    vista.lblPagoMensual.setText(String.valueOf(cot.calcularPagoMensual()));
                    isInsertar = false;
                    vista.btnBorrar.setEnabled(true);
                }
            } catch (Exception ex) {
            }

        }

        if (e.getSource() == vista.btnGuardar) {
            DbCotizacion db = new DbCotizacion();
            Cotizaciones cot = new Cotizaciones();

            cot.setNumCotizacion(vista.txtNumCotizacion.getText());
            cot.setDescripcionAutomovil(vista.txtDescripcion.getText());
            cot.setPorcentajePago(Integer.parseInt(vista.spnPorcentaje.getValue().toString()));
            cot.setPrecioAutomovil(Integer.parseInt(vista.txtPrecio.getText()));

            if (vista.rdb12.isSelected()) {
                cot.setPlazo(12);
            }
            if (vista.rdb24.isSelected()) {
                cot.setPlazo(24);
            }
            if (vista.rdb36.isSelected()) {
                cot.setPlazo(36);
            }
            if (vista.rdb48.isSelected()) {
                cot.setPlazo(48);
            }
            if (vista.rdb60.isSelected()) {
                cot.setPlazo(60);
            }
            try {
                if (isInsertar == true) {
                    db.insertar(cot);
                    JOptionPane.showMessageDialog(vista, "Se Añadio con Exito");

                } else {
                    db.actualizar(cot);
                    JOptionPane.showMessageDialog(vista, "Se Actulizo con Exito");
                    limpiar();
                    deshabilitar();
                }
            } catch (Exception ex) {

            }
        }
        
        if (e.getSource() == vista.btnCancelar) {
            limpiar();
            deshabilitar();
        }

        if (e.getSource() == vista.btnNuevo) {
            habilitar();
            isInsertar = true;
        }

        if (e.getSource() == vista.btnLimpiar) {
            limpiar();
        }
        cargarListaCotizacion();
    }
    
    public static void main(String[] args) {
        Cotizaciones cot = new Cotizaciones();
        DbCotizacion db = new DbCotizacion();
        dlgCotizacion vista = new dlgCotizacion(new JFrame(), true);
        Controlador contra = new Controlador(cot, vista, db);
        contra.iniciarVista();
    }

    }