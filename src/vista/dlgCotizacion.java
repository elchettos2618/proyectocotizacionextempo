/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package vista;

/**
 *
 * @author yh9pl
 */
public class dlgCotizacion extends javax.swing.JDialog {

    /**
     * Creates new form dlgCotizacion
     */
    public dlgCotizacion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgPlazos = new javax.swing.ButtonGroup();
        pnlDatosCotizacion = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNumCotizacion = new javax.swing.JTextField();
        btnBorrar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescripcion = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        spnPorcentaje = new javax.swing.JSpinner();
        pnlPlazos = new javax.swing.JPanel();
        rdb12 = new javax.swing.JRadioButton();
        rdb24 = new javax.swing.JRadioButton();
        rdb36 = new javax.swing.JRadioButton();
        rdb48 = new javax.swing.JRadioButton();
        rdb60 = new javax.swing.JRadioButton();
        btnBuscar = new javax.swing.JButton();
        pnCalculo = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblPagoInicial = new javax.swing.JLabel();
        lblTotalFin = new javax.swing.JLabel();
        lblPagoMensual = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtbListaAlumnos = new javax.swing.JTable();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        pnlDatosCotizacion.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlDatosCotizacion.setLayout(null);

        jLabel1.setText("Número de Cotización: ");
        pnlDatosCotizacion.add(jLabel1);
        jLabel1.setBounds(20, 30, 140, 16);
        pnlDatosCotizacion.add(txtNumCotizacion);
        txtNumCotizacion.setBounds(155, 29, 71, 22);

        btnBorrar.setText("Borrar");
        pnlDatosCotizacion.add(btnBorrar);
        btnBorrar.setBounds(360, 130, 90, 40);

        btnNuevo.setText("Nuevo");
        pnlDatosCotizacion.add(btnNuevo);
        btnNuevo.setBounds(360, 30, 90, 40);

        btnGuardar.setText("Guardar");
        pnlDatosCotizacion.add(btnGuardar);
        btnGuardar.setBounds(360, 80, 90, 40);

        jLabel2.setText("Descripción del Auto: ");
        pnlDatosCotizacion.add(jLabel2);
        jLabel2.setBounds(20, 68, 130, 16);

        txtDescripcion.setColumns(20);
        txtDescripcion.setRows(5);
        jScrollPane1.setViewportView(txtDescripcion);

        pnlDatosCotizacion.add(jScrollPane1);
        jScrollPane1.setBounds(20, 90, 320, 80);

        jLabel3.setText("Precio: ");
        pnlDatosCotizacion.add(jLabel3);
        jLabel3.setBounds(20, 190, 50, 16);
        pnlDatosCotizacion.add(txtPrecio);
        txtPrecio.setBounds(70, 189, 100, 22);

        jLabel4.setText("Porcentaje de Pago Inicial: ");
        pnlDatosCotizacion.add(jLabel4);
        jLabel4.setBounds(190, 190, 150, 16);

        spnPorcentaje.setModel(new javax.swing.SpinnerNumberModel(10, 10, 100, 1));
        pnlDatosCotizacion.add(spnPorcentaje);
        spnPorcentaje.setBounds(340, 190, 110, 22);

        pnlPlazos.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlPlazos.setLayout(null);

        bgPlazos.add(rdb12);
        rdb12.setSelected(true);
        rdb12.setText("12 Meses");
        pnlPlazos.add(rdb12);
        rdb12.setBounds(40, 15, 80, 21);

        bgPlazos.add(rdb24);
        rdb24.setText("24 Meses");
        pnlPlazos.add(rdb24);
        rdb24.setBounds(40, 40, 90, 21);

        bgPlazos.add(rdb36);
        rdb36.setText("36 Meses");
        pnlPlazos.add(rdb36);
        rdb36.setBounds(155, 15, 90, 21);

        bgPlazos.add(rdb48);
        rdb48.setText("48 Meses");
        pnlPlazos.add(rdb48);
        rdb48.setBounds(155, 40, 90, 21);

        bgPlazos.add(rdb60);
        rdb60.setText("60 Meses");
        pnlPlazos.add(rdb60);
        rdb60.setBounds(260, 30, 90, 21);

        pnlDatosCotizacion.add(pnlPlazos);
        pnlPlazos.setBounds(20, 230, 430, 80);

        btnBuscar.setText("Buscar");
        pnlDatosCotizacion.add(btnBuscar);
        btnBuscar.setBounds(240, 29, 90, 23);

        getContentPane().add(pnlDatosCotizacion);
        pnlDatosCotizacion.setBounds(10, 10, 470, 330);

        pnCalculo.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnCalculo.setLayout(null);

        jLabel5.setText("Pago Inicial:");
        pnCalculo.add(jLabel5);
        jLabel5.setBounds(30, 30, 80, 16);

        jLabel6.setText("Total a Financiar: ");
        pnCalculo.add(jLabel6);
        jLabel6.setBounds(30, 60, 110, 16);

        jLabel7.setText("Pago Mensual: ");
        pnCalculo.add(jLabel7);
        jLabel7.setBounds(30, 90, 90, 16);

        lblPagoInicial.setText("$ 0");
        pnCalculo.add(lblPagoInicial);
        lblPagoInicial.setBounds(150, 30, 150, 16);

        lblTotalFin.setText("$ 0");
        pnCalculo.add(lblTotalFin);
        lblTotalFin.setBounds(150, 60, 150, 16);

        lblPagoMensual.setText("$ 0");
        pnCalculo.add(lblPagoMensual);
        lblPagoMensual.setBounds(150, 90, 150, 16);

        getContentPane().add(pnCalculo);
        pnCalculo.setBounds(10, 350, 470, 140);

        jtbListaAlumnos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Num. Cotización", "Descripción", "Precio", "Porcentaje", "Plazo en Meses"
            }
        ));
        jScrollPane2.setViewportView(jtbListaAlumnos);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(10, 500, 470, 140);

        btnLimpiar.setText("Limpiar");
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(30, 650, 90, 23);

        btnCancelar.setText("Cancelar");
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(200, 650, 90, 23);

        btnCerrar.setText("Cerrar");
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(370, 650, 90, 23);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgCotizacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgCotizacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgCotizacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgCotizacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgCotizacion dialog = new dlgCotizacion(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.ButtonGroup bgPlazos;
    public javax.swing.JButton btnBorrar;
    public javax.swing.JButton btnBuscar;
    public javax.swing.JButton btnCancelar;
    public javax.swing.JButton btnCerrar;
    public javax.swing.JButton btnGuardar;
    public javax.swing.JButton btnLimpiar;
    public javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    public javax.swing.JTable jtbListaAlumnos;
    public javax.swing.JLabel lblPagoInicial;
    public javax.swing.JLabel lblPagoMensual;
    public javax.swing.JLabel lblTotalFin;
    private javax.swing.JPanel pnCalculo;
    private javax.swing.JPanel pnlDatosCotizacion;
    public javax.swing.JPanel pnlPlazos;
    public javax.swing.JRadioButton rdb12;
    public javax.swing.JRadioButton rdb24;
    public javax.swing.JRadioButton rdb36;
    public javax.swing.JRadioButton rdb48;
    public javax.swing.JRadioButton rdb60;
    public javax.swing.JSpinner spnPorcentaje;
    public javax.swing.JTextArea txtDescripcion;
    public javax.swing.JTextField txtNumCotizacion;
    public javax.swing.JTextField txtPrecio;
    // End of variables declaration//GEN-END:variables
}
