
package modelo;
import java.util.ArrayList;

/**
 *
 * @author quier
 */
public interface dbPersistencia {
    public void insertar(Object obj) throws Exception;
    public void actualizar(Object obj) throws Exception;
    public void borrar(Object obj) throws Exception;
    
    public Object consultar(String codigo) throws Exception;
    public Object[][] cargarDatos();
    
    public int numRegistros();
    public boolean isExiste(String codigo) throws Exception;
    
}
