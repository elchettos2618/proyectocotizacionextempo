
package modelo;
import java.sql.*;

/**
 *
 * @author quier
 */
public abstract class DbManejador {
    protected Connection conexion;
    protected String sqlConsulta;
    protected ResultSet registros;
    private String usuario;
    private String database;
    private String password;
    private String drive;
    private String url;

    public DbManejador(Connection conexion, String sqlConsulta, ResultSet registros, String usuario, String database, String password, String drive, String url) {
        this.conexion = conexion;
        this.sqlConsulta = sqlConsulta;
        this.registros = registros;
        this.usuario = usuario;
        this.database = database;
        this.password = password;
        this.drive = drive;
        this.url = url;
        isDrive();
    }
    
    public DbManejador() {
        this.drive = "com.mysql.cj.jdbc.Driver";
        this.database = "sistemas";
        this.usuario = "root";
        this.password = "";
        this.url = "jdbc:mysql://localhost/sistemas";
        isDrive();
    }
    
    public DbManejador(DbManejador otro) {
        this.conexion = otro.conexion;
        this.sqlConsulta = otro.sqlConsulta;
        this.registros = otro.registros;
        this.usuario = otro.usuario;
        this.database = otro.database;
        this.password = otro.password;
        this.drive = otro.drive;
        this.url = otro.url;
        isDrive();
    }
    
    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public String getSqlConsulta() {
        return sqlConsulta;
    }

    public void setSqlConsulta(String sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }

    public ResultSet getRegistros() {
        return registros;
    }

    public void setRegistros(ResultSet registros) {
        this.registros = registros;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDrive() {
        return drive;
    }

    public void setDrive(String drive) {
        this.drive = drive;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public boolean isDrive() {
        boolean exito = false;
        try {
            Class.forName(drive);
            exito = true;
        }
        catch(ClassNotFoundException e) {
            System.err.println("Surgió un error: " + e.getMessage());
            System.exit(-1);
        }
        return exito;
    }
    
    public boolean conectar() {
        boolean exito = false;
        try {
            this.setConexion(DriverManager.getConnection(this.url, this.usuario, this.password));
            exito = true;
        }
        catch(SQLException e) {
            System.err.println("Surgió un error: " + e.getMessage());
        }
        return exito;
    }
    
    public void desconectar() {
        try {
            if(!this.conexion.isClosed()) this.getConexion().close();
        }
        catch(SQLException e) {
            System.err.println("No se pudo cerrar la conexión. Error: " + e.getMessage());
        }
    }
}
